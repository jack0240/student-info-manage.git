import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * 学生信息管理系统
 *　博客地址：https://blog.csdn.net/WeiHao0240/article/details/120843764
 */
public class StudentInfoManage {
    public static void main(String args[]) {
        new FrameInOut();
    }
}

class FrameInOut extends JFrame implements ActionListener {
    JButton btn;   //定义按钮
    JPanel pb;     //定义面板作为容器
//    Image im = Toolkit.getDefaultToolkit().getImage(Panel.class.getResource("/s.jpg"));

    FrameInOut() {
        super("学生信息管理系统");  //设置界面标题
        btn = new JButton("进入学生信息系统");
        btn.setForeground(Color.red);
        btn.setFont(new Font("黑体", Font.BOLD, 18));
        pb = new JPanel();
        pb.add(btn);
        add(pb);
        setLayout(new FlowLayout());
        // pb.setBackground(Color.black);
        btn.addActionListener(this);
        setLayout(new FlowLayout());  //设置界面采用流式布局
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(320, 240);
        setVisible(true);
    }

    public void paint(Graphics g) {
        super.paint(g);
//        g.drawImage(im, 0, 0, 320, 240, this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btn) {
            new Information();//实例化
            setVisible(false);
        }
    }
}

class Information extends JFrame implements ActionListener {
    JMenuBar xx;//创建水平菜单栏xx

    JButton a, b, c, d, f;

    Information() {
        xx = new JMenuBar();

        /*初始化各个按钮*/

        a = new JButton("显示");
        b = new JButton("查询");
        c = new JButton("添加");
        d = new JButton("删除");
        f = new JButton("修改");

        /*将各控件按纽加入到菜单条xx中*/
        xx.add(a);
        xx.add(b);
        xx.add(c);
        xx.add(d);
        xx.add(f);

        setJMenuBar(xx);//设置菜单栏

        /*各个按钮注册事件监听器*/
        a.addActionListener(this);
        b.addActionListener(this);
        c.addActionListener(this);
        d.addActionListener(this);
        f.addActionListener(this);

        setTitle("学生信息管理系统");
        setBounds(100, 100, 400, 400);//设置在窗口的位置及大小
        setVisible(true);

    }

    /*重载ActionListener接口的方法，实现各按钮名副其实的功能*/
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == a)//如果动作的事件源是"显示"
        {
            new Show().actionPerformed(null);
        } else if (e.getSource() == b)//如果动作的事件源是"查询"
        {
            new Inquiry();
        } else if (e.getSource() == c)//如果动作的事件源是"添加"
        {
            new Add();
        } else if (e.getSource() == d)//如果动作的事件源是"删除"
        {
            new Delete();
        } else if (e.getSource() == f)//如果动作的事件源是"修改"
        {
            new Revise();
        }
    }
}

//学生信息的浏览
class Show extends JFrame implements ActionListener {
    JButton btn1;
    JTextArea ta1;  //定义一个文本域
    JPanel p1;
    Connection con;  //与数据源的连接
    Statement sql;  //在已经建立数据库连接的基础上，向数据库发送要执行的SQL语句
    ResultSet rs;  //执行查询数据库的语句生成

    Show() {
        btn1 = new JButton("显示基本信息");

        btn1.setForeground(Color.blue);  //设置按钮字体的颜色

        btn1.setFont(new Font("黑体", Font.BOLD, 12));  //设置按钮字体的大小

        ta1 = new JTextArea(5, 15);  //设置按文本域的大小

        ta1.setFont(new Font("黑体", Font.BOLD, 20));  //设置文本域所显示字体的大小

        btn1.addActionListener(this);  //对显示按钮进行监听

        p1 = new JPanel();

        p1.add(btn1);  //在面板上添加按钮

        p1.add(ta1);  //在面板上添加文件域

        getContentPane().add(p1);  //获得JFrame的内容面板，再对其加入组件

        setLayout(new FlowLayout());  //设置界面采用流式布局

        p1.setBackground(Color.pink);  //设置面板背景色

        //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setTitle("显示学生信息");

        setSize(1200, 360);  //设置界面尺寸
        setVisible(true);  //设置界面可见

    }

    /*重载ActionListener接口的方法*/
    @Override
    public void actionPerformed(ActionEvent e) {
        if (true) {
            // 清空内容
            ta1.setText("");
            try {
                Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");  //加载数据库驱动程序
            } catch (ClassNotFoundException ex) {
                System.out.println(ex.getMessage());
            }

            try {
                con = DriverManager.getConnection("jdbc:odbc:studentInfo", "", "");//数据源名称url,账号，密码 建立连接
                sql = con.createStatement(); //让已创建的连接对象con调用方法createStatement()创建这个SQL语句对象
                rs = sql.executeQuery("SELECT * FROM xinxibiao");//执行查询
                while (rs.next())//顺序查询数据
                {
                    String number = rs.getString(1);//文本区域的对象调用append() 方法，append方法在被选元素的结尾(仍然在内部)插入指定内容
                    String name = rs.getString(2);
                    String sex = rs.getString(3);
                    String place = rs.getString(4);
                    String score = rs.getString(5);
                    String phone = rs.getString(6);
                    String qq = rs.getString(7);
                    ta1.append("学号：" + number);
                    ta1.append("\t姓名：" + name);
                    ta1.append("\t性别：" + sex);
                    ta1.append("\t籍贯：" + place);
                    ta1.append("\t成绩：" + score);
                    ta1.append("\t电话：" + phone);
                    ta1.append("\tqq:" + qq + "\n");
                }
                con.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}

//学生信息的查询
class Inquiry extends JFrame implements ActionListener {
    JButton btn2;
    JTextArea ta2;
    JTextField tf1;
    JLabel lab1;//定义标签
    JPanel p2;
    Connection con;  //Connection代表打开的、与数据源的连接
    Statement sql;  //在已经建立数据库连接的基础上，向数据库发送要执行的SQL语句
    ResultSet rs;  //执行查询数据库的语句生成

    Inquiry() {
        lab1 = new JLabel("请输入要查询的学号");

        btn2 = new JButton("查询");
        btn2.setForeground(Color.red);

        btn2.setFont(new Font("微软雅黑", Font.BOLD, 12));

        ta2 = new JTextArea(10, 20);
        ta2.setForeground(Color.blue);
        ta2.setFont(new Font("黑体", Font.BOLD, 18));

        tf1 = new JTextField(10);

        p2 = new JPanel();

        add(lab1);//在面板上添加标签
        add(tf1);//在面板上添加文本域

        p2.add(btn2);//在面板上添加按纽
        p2.add(ta2);

        getContentPane().add(p2);//获得JFrame的内容面板，再对其加入组件
        setLayout(new FlowLayout());
        p2.setBackground(Color.blue);

        setTitle("查询学生信息");
        setBounds(20, 20, 400, 400);
        setVisible(true);
        btn2.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        boolean t = false;
        ta2.setText("");
        if (e.getSource() == btn2) {
            String str = tf1.getText();
            try {
                Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");//加载程序驱动器
            } catch (ClassNotFoundException ex) {
                System.out.println(ex.getMessage());
            }
            try {
                con = DriverManager.getConnection("jdbc:odbc:studentInfo", "", "");
                sql = con.createStatement();
                rs = sql.executeQuery("select * from xinxibiao where number like" + "'" + str + "'");//执行按学号查询
                //游标向下移动一行的意思 ，放在while循环里面做循环条件，就是如果有下一行的意思。并且执行了游标的向下移动
                while (rs.next()) {
                    String number = rs.getString(1);
                    String name = rs.getString(2);
                    String sex = rs.getString(3);
                    String place = rs.getString(4);
                    String score = rs.getString(5);
                    String phone = rs.getString(6);
                    String qq = rs.getString(7);

                    ta2.append("学号：" + number + "\n");
                    ta2.append("姓名：" + name + "\n");
                    ta2.append("性别：" + sex + "\n");
                    ta2.append("籍贯：" + place + "\n");
                    ta2.append("成绩：" + score + "\n");
                    ta2.append("电话：" + phone + "\n");
                    ta2.append("qq:" + qq + "\n");
                    t = true;
                }

                if (t == false)
                    ta2.append("输入有误");
                con.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}

//学生信息的添加
class Add extends JFrame implements ActionListener {
    JLabel lab2, lab3, lab4, lab5, lab6, lab7, lab8;//添加标签
    JTextField tf2, tf3, tf4, tf5, tf6, tf7, tf8;

    JButton btn3;
    JTextArea ta3;
    JPanel p3, p4;
    Connection con;  //Connection代表打开的、与数据源的连接
    Statement sql;  //在已经建立数据库连接的基础上，向数据库发送要执行的SQL语句
    ResultSet rs;  //执行查询数据库的语句生成

    //要添加的学生的信息
    Add() {
        lab2 = new JLabel("学号：");
        lab3 = new JLabel("姓名：");
        lab4 = new JLabel("性别：");
        lab5 = new JLabel("籍贯：");
        lab6 = new JLabel("成绩：");
        lab7 = new JLabel("电话：");
        lab8 = new JLabel("qq:");

        tf2 = new JTextField(10);
        tf3 = new JTextField(8);
        tf4 = new JTextField(4);
        tf5 = new JTextField(8);
        tf6 = new JTextField(5);
        tf7 = new JTextField(12);
        tf8 = new JTextField(12);

        ta3 = new JTextArea(10, 30);
        ta3.setForeground(Color.blue);
        ta3.setFont(new Font("黑体", Font.BOLD, 18));
        btn3 = new JButton("确认添加");
        btn3.addActionListener(this);

        btn3.setForeground(Color.red);
        btn3.setFont(new Font("微软雅黑", Font.BOLD, 12));

        p3 = new JPanel();
        p4 = new JPanel();

        add(lab2);
        add(tf2);
        add(lab3);
        add(tf3);
        add(lab4);
        add(tf4);
        add(lab5);
        add(tf5);
        add(lab6);
        add(tf6);
        add(lab7);
        add(tf7);
        add(lab8);
        add(tf8);

        p3.add(btn3);
        p4.add(ta3);

        getContentPane().add(p3);
        getContentPane().add(p4);

        setLayout(new FlowLayout());
        setTitle("添加学生信息");

        p3.setBackground(Color.blue);
        p4.setBackground(Color.yellow);

        setSize(1430, 364);
        setVisible(true);
        validate();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btn3) {
            String a2, a3, a4, a5, a6, a7, a8, updateStr, recode;
            a2 = tf2.getText();
            a3 = tf3.getText();
            a4 = tf4.getText();
            a5 = tf5.getText();
            a6 = tf6.getText();
            a7 = tf7.getText();
            a8 = tf8.getText();

            ta3.setText("");
            try {
                Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");//使用jdbc与odbc桥创建数据库的连接，加载驱动程序
            } catch (ClassNotFoundException ex) {
                System.out.println(ex.getMessage());
            }
            try {
                con = DriverManager.getConnection("jdbc:odbc:studentInfo", "", "");
                sql = con.createStatement();
                recode = "(" + "'" + a2 + "'" + "," + "'" + a3 + "'" + "," + "'" + a4 + "'" + "," + "'" + a5 + "'" + "," + "'" + a6 + "'" + "," + "'" + a7 + "'" + "," + "'" + a8 + "'" + ")";
                updateStr = "INSERT INTO xinxibiao VALUES" + recode;//在xinxibiao表中添加一条新的记录
                sql.executeUpdate(updateStr);//添加以后对数据库表中记录的字段进行更新
                rs = sql.executeQuery("SELECT * FROM xinxibiao");//执行查询语句

                while (rs.next()) {//获取信息表中的各列信息
                    String number = rs.getString(1);
                    String name = rs.getString(2);
                    String sex = rs.getString(3);
                    String place = rs.getString(4);
                    String score = rs.getString(5);
                    String phone = rs.getString(6);
                    String qq = rs.getString(7);

                    ta3.append("学号：" + number);//append() 方法在被选元素的结尾(仍然在内部)插入指定内容
                    ta3.append("\t姓名：" + name);
                    ta3.append("\t性别：" + sex);
                    ta3.append("\t籍贯：" + place);
                    ta3.append("\t成绩：" + score);
                    ta3.append("\t电话：" + phone);
                    ta3.append("\tqq:" + qq + "\n");
                }
                con.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}

//学生信息的删除
class Delete extends JFrame implements ActionListener {
    JButton btn4;
    JTextArea ta4;
    JTextField tf9;
    JLabel lab9;
    JPanel p5;
    Connection con;
    Statement sql;
    ResultSet rs;

    Delete() {
        lab9 = new JLabel("输入要删除者的学号");
        btn4 = new JButton("查询");
        btn4.setForeground(Color.red);
        btn4.setFont(new Font("微软雅黑", Font.BOLD, 12));
        ta4 = new JTextArea(10, 20);
        ta4.setForeground(Color.blue);
        ta4.setFont(new Font("黑体", Font.BOLD, 18));

        tf9 = new JTextField(10);
        p5 = new JPanel();
        add(lab9);
        add(tf9);

        p5.add(btn4);
        p5.add(ta4);

        getContentPane().add(p5);
        setLayout(new FlowLayout());
        p5.setBackground(Color.blue);

        setTitle("删除学生信息");
        setSize(1100, 390);
        setVisible(true);
        btn4.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        boolean t = false;
        ta4.setText("");

        if (e.getSource() == btn4) {
            String str = tf9.getText();
            try {
                Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            } catch (ClassNotFoundException ex) {
                System.out.println(ex.getMessage());
            }
            try {
                con = DriverManager.getConnection("jdbc:odbc:studentInfo", "", "");
                sql = con.createStatement();
                sql.executeUpdate("DELETE FROM xinxibiao WHERE number LIKE" + "'" + str + "'");
                rs = sql.executeQuery("select * from xinxibiao");

                while (rs.next()) {
                    String number = rs.getString(1);
                    String name = rs.getString(2);
                    String sex = rs.getString(3);
                    String place = rs.getString(4);
                    String score = rs.getString(5);
                    String phone = rs.getString(6);
                    String qq = rs.getString(7);

                    ta4.append("学号：" + number);
                    ta4.append("\t姓名：" + name);
                    ta4.append("\t性别：" + sex);
                    ta4.append("\t籍贯：" + place);
                    ta4.append("\t成绩：" + score);
                    ta4.append("\t电话：" + phone);
                    ta4.append("\tqq:" + qq + "\n");
                    t = true;
                }
                if (t == false)
                    ta4.append("输入有误");
                con.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}

//学生信息的修改
class Revise extends JFrame implements ActionListener {
    JLabel lab10, lab11, lab12, lab13, lab14, lab15, lab16;
    JTextField tf10, tf11, tf12, tf13, tf14, tf15, tf16;

    JButton btn5;
    JTextArea ta11;
    JPanel p11, p12;
    Connection con;//Connection代表打开的、与数据源的连接
    Statement sql;//在已经建立数据库连接的基础上，向数据库发送要执行的SQL语句
    ResultSet rs;//执行查询数据库的语句生成

    Revise() {
        lab10 = new JLabel("学号：");
        lab11 = new JLabel("姓名：");
        lab12 = new JLabel("性别：");
        lab13 = new JLabel("籍贯：");
        lab14 = new JLabel("成绩：");
        lab15 = new JLabel("电话：");
        lab16 = new JLabel("qq:");

        tf10 = new JTextField(10);
        tf11 = new JTextField(8);
        tf12 = new JTextField(4);
        tf13 = new JTextField(8);
        tf14 = new JTextField(5);
        tf15 = new JTextField(12);
        tf16 = new JTextField(12);

        ta11 = new JTextArea(10, 30);//文本域的大小
        ta11.setForeground(Color.blue);
        ta11.setFont(new Font("黑体", Font.BOLD, 18));
        btn5 = new JButton("确认修改");
        btn5.addActionListener(this);

        btn5.setForeground(Color.red);
        btn5.setFont(new Font("微软雅黑", Font.BOLD, 12));

        p11 = new JPanel();
        p12 = new JPanel();

        add(lab10);
        add(tf10);
        add(lab11);
        add(tf11);
        add(lab12);
        add(tf12);
        add(lab13);
        add(tf13);
        add(lab14);
        add(tf14);
        add(lab15);
        add(tf15);
        add(lab16);
        add(tf16);

        p11.add(btn5);
        p12.add(ta11);

        getContentPane().add(p11);
        getContentPane().add(p12);

        setLayout(new FlowLayout());
        setTitle("修改学生信息");

        p11.setBackground(Color.blue);
        p12.setBackground(Color.yellow);

        setSize(1430, 364);
        setVisible(true);
        validate();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btn5) {
            String a10, a11, a12, a13, a14, a15, a16, updateStr;

            a10 = tf10.getText();
            a11 = tf11.getText();
            a12 = tf12.getText();
            a13 = tf13.getText();
            a14 = tf14.getText();
            a15 = tf15.getText();
            a16 = tf16.getText();

            ta11.setText("");//设置文本域的文本为空
            try {
                Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            } catch (ClassNotFoundException ex) {
                System.out.println(ex.getMessage());
            }
            try {
                con = DriverManager.getConnection("jdbc:odbc:studentInfo", "", "");
                //使用java.sql包中的Connection类声明一个对象，然后再使用类DriverManager调用它的静态方法创建连接对象
                sql = con.createStatement();

                updateStr = "update xinxibiao set name='" + a11 + "', sex='" + a12 + "',place='" + a13 + "',score='" + a14 + "', phone='" + a15 + "',qq='" + a16 + "' where number='" + a10 + "'";//按学号进行修改
                sql.executeUpdate(updateStr);//修改以后对数据库表中的字段进行更新

                rs = sql.executeQuery("SELECT * FROM xinxibiao");//SQL语句对数据库的查询操作将返回一个ResultSet对象

                while (rs.next()) {
                    String number = rs.getString(1);
                    String name = rs.getString(2);
                    String sex = rs.getString(3);
                    String place = rs.getString(4);
                    String score = rs.getString(5);
                    String phone = rs.getString(6);
                    String qq = rs.getString(7);

                    ta11.append("学号：" + number);
                    ta11.append("\t姓名：" + name);
                    ta11.append("\t性别：" + sex);
                    ta11.append("\t籍贯：" + place);
                    ta11.append("\t成绩：" + score);
                    ta11.append("\t电话：" + phone);
                    ta11.append("\tqq:" + qq + "\n");
                }
                con.close();//关闭数据库连接
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}


# 基于Java Swing的学生信息管理系统

## 介绍
有新生来报到，要逐个录入其信息，如：学生姓名，性别，成绩，不是很复杂的小型客户开发。
本程序基于jdbc实现了对学生信息的添加、查询、修改、删除等操作，
同时支持查看所有学生信息，功能完善，界面简洁美观，布局合理，操作简便，简单易用，任何人可轻松操作。


## 相关技术
1.  Java的Swing编程
2.  Java的JDBC编程


## 所需环境
1.  JDK1.7
2.  Access 2016


## 安装教程
遇到问题可以到**相关博客**进行查看

1.  **运行之前需要配置好ODBC**
2.  检查JDK版本，必须是JDK1.7：
```
java -version
```

3.  进入`src`目录，编译
```
javac -encoding UTF-8 StudentInfoManage.java
```
![输入图片说明](https://images.gitee.com/uploads/images/2021/1019/121603_98e6f50e_1590078.png "0.进入src.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1019/121612_f1e071f2_1590078.png "0.编译.png")

4.  运行
```
java StudentInfoManage
```
![输入图片说明](https://images.gitee.com/uploads/images/2021/1019/121623_02521f78_1590078.png "0.运行.png")

注意：如果修改源代码后需要删除所有.class文件，在编译运行！


## 运行截图

1.  首页

![输入图片说明](https://images.gitee.com/uploads/images/2021/1019/121637_d88f5cfe_1590078.png "1.首页.png")

2.  菜单栏

![输入图片说明](https://images.gitee.com/uploads/images/2021/1019/121643_90307c14_1590078.png "2.菜单栏.png")

3.  显示全部

![输入图片说明](https://images.gitee.com/uploads/images/2021/1019/121652_1198e08d_1590078.png "3.显示全部.png")

4.  查询单个

![输入图片说明](https://images.gitee.com/uploads/images/2021/1019/121658_c9af1434_1590078.png "4.查询单个.png")

5.  添加

![输入图片说明](https://images.gitee.com/uploads/images/2021/1019/121704_35398d7f_1590078.png "5.添加.png")

6.  删除

![输入图片说明](https://images.gitee.com/uploads/images/2021/1019/121714_30dd8395_1590078.png "6.删除1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1019/121722_0e613143_1590078.png "6.删除2.png")

7.  修改

![输入图片说明](https://images.gitee.com/uploads/images/2021/1019/121729_5b983969_1590078.png "7.修改.png")


## 相关博客
1.  [Microsoft Access 2016安装教程](https://blog.csdn.net/WeiHao0240/article/details/120672363)
2.  [Java使用ODBC连接Access数据库](https://blog.csdn.net/WeiHao0240/article/details/120727203)
3.  [TextPad安装环境配置](https://jackwei.blog.csdn.net/article/details/86914950)
4.  [IDEA运行Java Swing项目中文乱码](https://blog.csdn.net/WeiHao0240/article/details/120744954)
5.  [Java指令编译java文件](https://blog.csdn.net/WeiHao0240/article/details/120778832)